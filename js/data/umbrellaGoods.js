(function(window){
    //goods data
    this.goods = [  {id: "0", color: "purple", price: "50"}, 
                    {id: "1", color: "dark", price: "50"},
                    {id: "2", color: "pink", price: "60"}];
    var goodsInfo = function(){
        return this.goods;
    }
    
    //set a scope
    window.data = window.data || {};
    window.data.goods = goodsInfo();
})(window);