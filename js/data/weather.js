(function(window){
    //static data
    this.api = "https://api.openweathermap.org/data/2.5/weather?q=";
    this.key = "9c39fa3ce9d953fdd507d7d9f77093ef";
    this.cityName = "Hong Kong";
    this.refreshInterval = 12000;
    var apiUrl = function(){
        return this.api + this.cityName + "&appid=" + this.key;
    }
    
    //set a scope
    window.data = window.data || {};
    window.data.weatherUrl = apiUrl();
    window.data.cityName = this.cityName;
    window.data.weatherRefreshInterval = this.refreshInterval;
})(window);