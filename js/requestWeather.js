var updateWeather = function(){
   $.getJSON(window.data.weatherUrl, function(hkWeather){ 
       var message = window.data.cityName + ". ";
      if (hkWeather.weather && hkWeather.weather.main && hkWeather.weather.main === "rain"){
          message += "It is POURING! You may need an UMBRELLA!";
      }else{
          message += "Though it is not pouring outside now, it will someday. You may need an UMBRELLA!";
      }
      $('#RainOrNot').text(message);
    } );}
    
$('document').ready(function(){
    updateWeather();
    setInterval(updateWeather, window.data.weatherRefreshInterval); //update the weather every 2 mins
});