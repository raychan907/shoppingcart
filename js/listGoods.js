{
    let selected = [];
    let unselected = [];
    let goods = window.data.goods;
for (let i = 0; i < window.data.goods.length; i++)
{
    $('#goods').append('<button id="' + goods[i].id + '" '+
                        'class="unselected">' +
                        'Umbrella in ' + goods[i].color + '- $' +
                        goods[i].price + '</button>');
    unselected.push(goods[i].id);
}
$(document).ready(function(){
    //have this text to be shown when the document is ready
    //so that the time of displaying this would be very close to the time of displaying other elements
    $("#cartTitle").text("Good(s) in your cart:");  
    
    $("button").click(function(event){
        var id = event.target.id;
        if (unselected.indexOf((new String(id)).trim()) != -1){
            unselected.splice(unselected.indexOf(id), 1);
            selected.push(id);
            $('#' + id).removeClass('unselected');
            $('#' + id).addClass('selected');
        }else{
            selected.splice(selected.indexOf(id), 1);
            unselected.push(id);
            $('#' + id).removeClass('selected');
            $('#' + id).addClass('unselected');
        }
        $("#goodsInCart").empty();
        for (let i = 0; i < selected.length; i++){
            for (let j = 0; j < goods.length; j++){
                if (goods[j].id === selected[i]){
                    $("#goodsInCart").append('<li>' + goods[j].color + "- $" + goods[j].price + '</li>');
                    break;
                }
            }
        }
    });
});
}